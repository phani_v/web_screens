import { INavData } from '@coreui/angular';
import { Router, ActivatedRoute } from '@angular/router';
//import { url } from 'inspector';
// let utente = JSON.parse(localStorage.getItem('rolename'));

export class navcompoenent {
  constructor(public route: ActivatedRoute) {

  }
  // coderitems:navItemsCoder;
  hidenavitems: boolean;
  ngoninit() {
    if (this.route.routeConfig.component.name == 'ChartComponent') { this.hidenavitems = true }
    else this.hidenavitems = null;
  }

}
export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'

    // badge: {
    //   variant: 'info',
    //   text: 'NEW'
    // }
  },
  {
    name: 'Products',
    url: '/products',
    icon: 'icon-film',
    children: [
      {
      name: 'Product Categories',
      url: '/'
      },
      {
      name: 'Product Types',
      url: '/'
      },
      {
      name: 'Product',
      url: '/'
      },
      {
      name: 'Product Components',
      url: '/'
      },
      {
      name: 'Product Recall',
      url: '/'
      },
      ]
  },
  {
    name: 'Assets',
    url: '/assets',
    icon: 'icon-organization',
    children: [
      {
      name: 'Asset Groups',
      url: '/'
      },
      {
      name: 'Assets',
      url: '/'
      },
      {
      name: 'Asset Parts',
      url: '/'
      },
      ]
  },
  {
    name: 'Maintenance',
    url: '/maintenance',
    icon: 'icon-wrench',
    children: [
      {
      name: 'Maintenance Category',
      url: '/maintenance/category'
      },
      {
      name: 'Maintenance Schedule',
      url: '/maintenance/schedule'
      },
      {
      name: 'Maintenance Requests',
      url: '/maintenance/requests'
      },
      {
        name: 'Maintenance assets',
        url: '/maintenance/assets'
        },
        {
        name: 'Maintenance Checklist',
        url: '/maintenance/checklist'
        },
        {
          name: 'Task',
          url: '/maintenance/task'
          },
      ]
  },
  {
    name: 'Ticketing',
    url: '/ticketing',
    icon: 'icon-tag'
  },
  {
    name: 'Incident',
    url: '/incident',
    icon: 'icon-loop'
  },
  {
    name: 'Asset Alerts',
    url: '/asset-alerts',
    icon: 'icon-exclamation'
  },
  {
    name: 'Maps',
    url: '/maps',
    icon: 'icon-map'
  },
  {
    name: 'Infrastructure',
    url: '/infrastructure',
    icon: 'icon-support',
    children: [
      {
      name: 'Readers',
      url: '/'
      },
      {
      name: 'Receivers',
      url: '/'
      },
      {
      name: 'RFID Tags',
      url: '/'
      },
      {
        name: 'Antennas',
        url: '/'
        },
        {
        name: 'Locations',
        url: '/'
        },
      ]
  },
  {
    name: 'Environment-Monitoring',
    url: '/environment-monitoring',
    icon: 'icon-organization',
    children: [
      {
      name: 'Overview',
      url: '/'
      },
      {
      name: 'Monitors',
      url: '/'
      },
      {
      name: 'Reports',
      url: '/'
      },
      {
        name: 'Configuration',
        url: '/'
        },
        {
        name: 'Alerts History',
        url: '/'
        },
      ]
  },
  {
    name: 'Facility',
    url: '/facility',
    icon: 'icon-frame'
  },
  {
    name: 'Department',
    url: '/department',
    icon: 'icon-folder'
  },
  {
    name: 'Users',
    url: '/users',
    icon: 'icon-user-following'
  },
  {
    name: 'Groups',
    url: '/groups',
    icon: 'icon-layers'
  },
  {
    name: 'Roles',
    url: '/roles',
    icon: 'icon-drawer'
  },
  {
    name: 'Contacts',
    url: '/contacts',
    icon: 'icon-notebook'
  },
  {
    name: 'Data-Upload',
    url: '/data-upload',
    icon: 'icon-cloud-upload'
  },
];




