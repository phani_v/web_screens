import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import layouts
import { DefaultLayoutComponent } from './layout';
import { LoginComponent } from './views/login/login.component';
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule),
    pathMatch: 'full',

  },
  {
    path: 'login',
    loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule),
    data: {
      title: 'Login Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'maintenance/category',
        loadChildren: () => import('./views/maintenance/maintenance-category/maintenance-category.module').then(m => m.MaintenanceCategoryModule)
      },
      {
        path: 'maintenance/schedule',
        loadChildren: () => import('./views/maintenance/maintenance-schedule/maintenance-schedule.module').then(m => m.MaintenanceScheduleModule)
      },
      {
        path: 'maintenance/requests',
        loadChildren: () => import('./views/maintenance/maintenance-requests/maintenance-requests.module').then(m => m.MaintenanceRequestsModule)
      },
      {
        path: 'maintenance/assets',
        loadChildren: () => import('./views/maintenance/maintenance-assets/maintenance-assets.module').then(m => m.MaintenanceAssetsModule)
      },
      {
        path: 'maintenance/checklist',
        loadChildren: () => import('./views/maintenance/maintenance-checklist/maintenance-checklist.module').then(m => m.MaintenanceChecklistModule)
      },
      {
        path: 'maintenance/task',
        loadChildren: () => import('./views/maintenance/maintenance-task/maintenance-task.module').then(m => m.MaintenanceTaskModule)
      },
    ]
  }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
