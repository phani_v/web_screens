import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceTaskComponent } from './maintenance-task.component';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceTaskComponent,
    data: {
      title: 'Maintenance Task'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceTaskRoutingModule { }
