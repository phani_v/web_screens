import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceTaskRoutingModule } from './maintenance-task-routing.module';
import { MaintenanceTaskComponent } from './maintenance-task.component';


@NgModule({
  declarations: [MaintenanceTaskComponent],
  imports: [
    CommonModule,
    MaintenanceTaskRoutingModule
  ]
})
export class MaintenanceTaskModule { }
