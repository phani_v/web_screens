import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceAssetsRoutingModule } from './maintenance-assets-routing.module';
import { MaintenanceAssetsComponent } from './maintenance-assets.component';


@NgModule({
  declarations: [MaintenanceAssetsComponent],
  imports: [
    CommonModule,
    MaintenanceAssetsRoutingModule
  ]
})
export class MaintenanceAssetsModule { }
