import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceAssetsComponent } from './maintenance-assets.component';

describe('MaintenanceAssetsComponent', () => {
  let component: MaintenanceAssetsComponent;
  let fixture: ComponentFixture<MaintenanceAssetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaintenanceAssetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
