import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceAssetsComponent } from './maintenance-assets.component';
const routes: Routes = [
  {
    path: '',
    component: MaintenanceAssetsComponent,
    data: {
      title: 'Maintenance Assets'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceAssetsRoutingModule { }
