import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceChecklistRoutingModule } from './maintenance-checklist-routing.module';
import { MaintenanceChecklistComponent } from './maintenance-checklist.component';


@NgModule({
  declarations: [MaintenanceChecklistComponent],
  imports: [
    CommonModule,
    MaintenanceChecklistRoutingModule
  ]
})
export class MaintenanceChecklistModule { }
