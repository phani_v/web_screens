import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceChecklistComponent } from './maintenance-checklist.component';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceChecklistComponent,
    data: {
      title: 'Maintenance Checklist'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceChecklistRoutingModule { }
