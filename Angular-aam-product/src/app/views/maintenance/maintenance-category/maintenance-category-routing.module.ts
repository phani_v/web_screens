import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceCategoryComponent } from './maintenance-category.component';
const routes: Routes = [
  {
    path: '',
    component: MaintenanceCategoryComponent,
    data: {
      title: 'Maintenance Category'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceCategoryRoutingModule { }
