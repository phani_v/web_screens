import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceCategoryRoutingModule } from './maintenance-category-routing.module';
import { MaintenanceCategoryComponent } from './maintenance-category.component';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MaintenanceCategoryComponent],
  imports: [
    CommonModule,
    MaintenanceCategoryRoutingModule,
    TableModule,
    FormsModule
  ]
})
export class MaintenanceCategoryModule { }
