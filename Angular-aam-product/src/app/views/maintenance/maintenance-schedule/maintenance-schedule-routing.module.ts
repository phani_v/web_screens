import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceScheduleComponent } from './maintenance-schedule.component';
const routes: Routes = [
  {
    path: '',
    component: MaintenanceScheduleComponent,
    data: {
      title: 'Maintenance Schedule'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceScheduleRoutingModule { }
