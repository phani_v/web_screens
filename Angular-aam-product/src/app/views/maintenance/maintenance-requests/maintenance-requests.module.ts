import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule} from '@angular/common';
import { MaintenanceRequestsRoutingModule } from './maintenance-requests-routing.module';
import { MaintenanceRequestsComponent } from './maintenance-requests.component';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {ContextMenuModule} from 'primeng/contextmenu';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { CarouselModule } from 'primeng/carousel';
import {MenubarModule} from 'primeng/menubar';
@NgModule({
  declarations: [MaintenanceRequestsComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    MaintenanceRequestsRoutingModule,
    TableModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    HttpClientModule,
    DropdownModule,
    MultiSelectModule,
    ContextMenuModule,
    ButtonModule,
    CalendarModule,
    SplitButtonModule,
    CarouselModule,
    MenubarModule
  ]
})
export class MaintenanceRequestsModule { }
