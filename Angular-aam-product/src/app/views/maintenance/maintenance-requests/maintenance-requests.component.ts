import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { CustomerService } from '../../../customerservice';
import { Customer } from '../../../customer';
import { LazyLoadEvent } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import { HttpClient } from '@angular/common/http';
import {MenuItem} from 'primeng/api';


@Component({
  selector: 'app-maintenance-requests',
  templateUrl: './maintenance-requests.component.html',
  styleUrls: ['./maintenance-requests.component.scss']
})
export class MaintenanceRequestsComponent implements OnInit {

  datasource: Customer[];
  gfg: MenuItem[];
    customers: Customer[];
    totalRecords: number;
    cols: any[];
    loading: boolean;
    name: string;
    code: string;
   // representatives: Representative[];

    selectAll: boolean = false;

    selectedCustomers: Customer[];
  constructor( public route: ActivatedRoute,public _router: Router,private cdRef: ChangeDetectorRef,private customerService: CustomerService, private primengConfig: PrimeNGConfig,private http: HttpClient) { 

  }

  ngOnInit() {
    this.gfg = [
      {
        label: 'Export',
        items: [
          {
            label: 'Microsoft Excel (.xlsx)'
          },
          {
            label: 'Document (.pdf)'
          }
        ]
      }
    ];
    this.customerService.getCustomersLarge().then(data => {
        this.datasource = data;
        this.totalRecords = data.length;
    });

    this.loading = true;
    this.primengConfig.ripple = true;
}
loadCustomers(event: LazyLoadEvent) {  
  this.loading = true;

  //in a real application, make a remote request to load data using state metadata from event
  //event.first = First row offset
  //event.rows = Number of rows per page
  //event.sortField = Field name to sort with
  //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
  //filters: FilterMetadata object having field as key and filter value, filter matchMode as value

  //imitate db connection over a network
  setTimeout(() => {
      if (this.datasource) {
          this.customers = this.datasource.slice(event.first, (event.first + event.rows));
          this.loading = false;
          this.totalRecords = this.customers.length;
      }
  }, 1000);
}

}
