import { Component, OnInit } from '@angular/core';
import {  FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewChild } from '@angular/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
public showresetSection:boolean = false;
public showloginSection:boolean = true;
public showsetpassword:boolean = false;
public showotpSection:boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(){
  }
  reset(){
    this.showloginSection = false;
    this.showresetSection = true;
    this.showsetpassword = false;
    this.showotpSection = false;
  }
  backtologin(){
    this.showloginSection = true;
    this.showresetSection = false;
    this.showsetpassword = false;
    this.showotpSection = false;
  }
  passwordreset(){
    this.showsetpassword = true;
    this.showloginSection = false;
    this.showresetSection = false;
    this.showotpSection = false;
  }
  passwordsubmit(){
    this.showsetpassword = false;
    this.showloginSection = false;
    this.showresetSection = false;
    this.showotpSection = true;
  }
}
