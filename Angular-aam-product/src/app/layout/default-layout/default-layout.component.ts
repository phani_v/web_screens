import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { navItems } from '../../_nav';
import {MenuItem} from 'primeng/api';
import {MenuModule} from 'primeng/menu';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  items: MenuItem[];
  constructor(
    public route: ActivatedRoute,
    public _router: Router,
    private cdRef: ChangeDetectorRef
  ) {

  }
  ngAfterViewChecked() {
    //let show = this.isShowExpand();
    //if (show != this.show) { // check if it change, tell CD update view
      //this.show = show;
      this.cdRef.detectChanges();
    }
  
  public sidebarMinimized = false;

  public navItems = navItems;
  
title:any;
hidenavitems:boolean;
isvisible:boolean;
Clientname:any;
  ngOnInit() {
    
    this.items = [
      {label: 'Profile'},
      {label: 'Settings'},
      {label: 'Logout'}
  ];

  }
  userName: string = localStorage.getItem("userName");
Menutitle:any;
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  

}
